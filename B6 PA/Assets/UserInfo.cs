﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserInfo
{
    private static UserInfo instance;
    private UserModel _user;
    private bool _soundFx;

    private UserInfo()
    {

    }

    public static UserInfo Instance
    {
        get
        {
            if(instance==null)
            {
                instance = new UserInfo();
            }
            return instance;
        }
    }

    public UserModel User
    {
        get { return _user; }
        set { _user = value; }
    }

    public bool SoundFx
    {
        get { return _soundFx; }
        set { _soundFx = value; }
    }
}
