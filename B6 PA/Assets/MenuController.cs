﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    [SerializeField] private Button btn_Play;
    [SerializeField] private InputField inp_name;

    private void Start()
    {
        btn_Play.interactable = false;
        UserInfo player = UserInfo.Instance;
        UserModel user = new UserModel();
        UserInfo.Instance.User = user;
        UserInfo.Instance.SoundFx = true;
    }

    public void Validate()
    {
        if (!string.IsNullOrEmpty(inp_name.text))
        {
            SetName();
            btn_Play.interactable = true;
        }
        else
        {
            btn_Play.interactable = false;
        }
    }

    private void ActivateSound()
    {
        bool activate = UserInfo.Instance.SoundFx;
        UserInfo.Instance.SoundFx = !activate;
        Debug.LogWarning("SoundFx: " + UserInfo.Instance.SoundFx);
    }

    private void SetName()
    {
        UserInfo.Instance.User.Name = inp_name.text;
    }

    public void NextScene(string scene)
    {
        SceneManager.LoadScene(scene, LoadSceneMode.Single);
    }
    
}
