﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Efects : MonoBehaviour
{
    public AudioSource Efe;
    public AudioClip encima;
    public AudioClip click;

    // Start is called before the first frame update
    public void OverS()
    {
        Efe.PlayOneShot(encima);
    }

    // Update is called once per frame
    public void ClickS()
    {
        Efe.PlayOneShot(click);
    }
}
