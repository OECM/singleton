﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    [SerializeField]public Text txt_name;
    [SerializeField]private string saludo = "Hola";

    private void Awake()
    {
        bool activate = UserInfo.Instance.SoundFx;
        if(activate)
        {
            Debug.LogWarning("Sonidos Activados");
        }
        else
        {
            Debug.LogWarning("Sonidos Desactivados");
        }
    }

    void Start()
    {
        Debug.Log("GameName: " + UserInfo.Instance.User.Name);
        txt_name.text = saludo + " " + UserInfo.Instance.User.Name;
    }
}
